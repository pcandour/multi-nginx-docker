Simple docker-compose script that allows you to have a single Nginx instance running multiple websites
that use PHP and MySQL.

Tested on Ubuntu 20.04 LTS and works fine with 1GB RAM, such as the free tier offered by Oracle Cloud.

Uses docker and docker-compose, which for Ubuntu installs like:

```
apt-get update
apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose
```

If you want to be able to run docker commands as not root, you can do this:

```
usermod -aG docker ubuntu
usermod -aG sudo ubuntu
```

Next lets add a bit of swap. Not sure if it's necessary, but probably not a bad idea:

```
fallocate -l 1G /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
```

Then edit /etc/fstab and put this in:

```
/swapfile none swap sw 0 0
```

Now put these files somewhere, create a directory to store your sites and MySQL (default is /data/www and /data/mysql)
You will need to have a file in the images/nginx/sites directory for each website, and then reference them in the
docker-compose.yaml file. In the section that starts 'php-fpm:' add a line in the volumes section that looks like:

```
      - /data/www/sitename.com:/var/www/sitename.com
```

And then do:

```
docker-compose up
```

Which will create your images and start them running. If you want to background the jobs so they keep running, do:

```
docker-compose up -d
```

Your websites can talk to the database using the hostname 'mysql'
